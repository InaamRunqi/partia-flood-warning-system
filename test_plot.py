from floodsystem.station import MonitoringStation
from floodsystem.plot import plot_water_levels, plot_water_level_with_fit
from datetime import datetime, timedelta

def test_plot_water_levels():
     """Test plot_water_levels in plot.py"""
     t = [datetime(2016, 12, 30), datetime(2016, 12, 31), datetime(2017, 1, 1),
         datetime(2017, 1, 2), datetime(2017, 1, 3), datetime(2017, 1, 4),
         datetime(2017, 1, 5)]
     level = [0.2, 0.7, 0.95, 0.92, 1.02, 0.91, 0.64]
     station = MonitoringStation(
                                station_id="station_id",
                                measure_id="measure_id",
                                label="label",
                                coord=(50,50),
                                typical_range=(0.2,0.95),
                                river="river",
                                town="town"
                               )
     plot_water_levels(station,t,level,)
     assert 1+1 == 2


def test_plot_water_level_with_fit():
     """Test plot_water_level_with_fit in plot.py"""
     t = [datetime(2016, 12, 30), datetime(2016, 12, 31), datetime(2017, 1, 1),
         datetime(2017, 1, 2), datetime(2017, 1, 3), datetime(2017, 1, 4),
         datetime(2017, 1, 5)]
     level = [0.2, 0.7, 0.95, 0.92, 1.02, 0.91, 0.64]
     station = MonitoringStation(
                                station_id="station_id",
                                measure_id="measure_id",
                                label="label",
                                coord=(50,50),
                                typical_range=(0.2,0.95),
                                river="river",
                                town="town"
                               )
     plot_water_level_with_fit(station,t,level,1)
     assert 1+1 == 2
