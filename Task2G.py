from floodsystem.flood import stations_level_over_threshold
from floodsystem.plot import plot_water_level_with_fit
from floodsystem.datafetcher import fetch_measure_levels
from datetime import datetime, timedelta
from floodsystem.stationdata import build_station_list, update_water_levels
import matplotlib
import numpy as np

def run():
    """Requirements for task 2G."""
    stations = build_station_list()
    update_water_levels(stations)
    stations = stations_level_over_threshold(stations,1.5)

    dt = 2
    
    for station in stations:
        dates, levels = fetch_measure_levels(station[0].measure_id,dt=timedelta(days=dt))
    
        x = matplotlib.dates.date2num(dates)
        y = levels

        #Find coefficients of best-fit polynomial f(x) of degree p
        if len(x) > 2:
            p_coeff = np.polyfit(x-x[-1],y,1)
            p_coeff = p_coeff[0]
            if p_coeff > 1.5:
                print(station[0].name,"risk: severe")
            elif p_coeff > 1.2:
                print(station[0].name,"risk: high")
            elif p_coeff > 1:
                print(station[0].name,"risk: moderate")
            else:
                print(station[0].name,"risk: low")
if __name__ == "__main__":
    print("*** Task 2G Part 1A Flood Warning System ***")
    run()