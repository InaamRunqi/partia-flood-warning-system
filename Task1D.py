import floodsystem.geo
from floodsystem.stationdata import build_station_list

def run():
    """Requirements for task 1D."""
    stations = build_station_list()
    rivers = floodsystem.geo.rivers_with_station(stations)
    num = len(rivers)
    print(num)
    dic = floodsystem.geo.stations_by_river(stations)
    def print_stations(river):
        """Print the names of stations on specified rivers."""
        stations = []
        for i in range(len(dic[river])):
            stations.append(dic[river][i].name)
        print(sorted(stations))
    print_stations("River Aire")
    print_stations("River Cam")
    print_stations("River Thames")

if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")
    run()