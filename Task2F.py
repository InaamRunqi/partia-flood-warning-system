from floodsystem.flood import stations_highest_rel_level
from floodsystem.plot import plot_water_level_with_fit
from floodsystem.datafetcher import fetch_measure_levels
from datetime import datetime, timedelta
from floodsystem.stationdata import build_station_list, update_water_levels

def run():
    """Requirements for task 2F."""
    stations = build_station_list()
    update_water_levels(stations)
    stations = stations_highest_rel_level(stations,5)

    dt = 2
    
    for station in stations:
        dates, levels = fetch_measure_levels(station[0].measure_id,dt=timedelta(days=dt))
        plot_water_level_with_fit(station[0],dates,levels,4)
    
if __name__ == "__main__":
    print("*** Task 2F Part 1A Flood Warning System ***")
    run()


