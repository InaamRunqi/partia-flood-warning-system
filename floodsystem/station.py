# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module provides a model for a monitoring station, and tools
for manipulating/modifying station data

"""


class MonitoringStation:
    """This class represents a river level monitoring station"""

    def __init__(self, station_id, measure_id, label, coord, typical_range,
                 river, town):

        self.station_id = station_id
        self.measure_id = measure_id

        # Handle case of erroneous data where data system returns
        # '[label, label]' rather than 'label'
        self.name = label
        if isinstance(label, list):
            self.name = label[0]

        self.coord = coord
        self.typical_range = typical_range
        self.river = river
        self.town = town

        self.latest_level = None

    def __repr__(self):
        d = "Station name:     {}\n".format(self.name)
        d += "   id:            {}\n".format(self.station_id)
        d += "   measure id:    {}\n".format(self.measure_id)
        d += "   coordinate:    {}\n".format(self.coord)
        d += "   town:          {}\n".format(self.town)
        d += "   river:         {}\n".format(self.river)
        d += "   typical range: {}".format(self.typical_range)
        return d

    def typical_range_consistent(self):
        "adresses errors caused by stations with inconsistent data for typical low/high ranges"
        #checks if there is data available for the typical range
        if self.typical_range == None:
            return False
        #checks that the reported typical high range is not less than the reported typical low
        if self.typical_range [0]>self.typical_range[1]:
            return False
    
    def relative_water_level(self):
        "returns latest water level as a fraction of the typical range"
        if self.typical_range_consistent() != False and self.latest_level != None: 
            a = self.latest_level - self.typical_range[0]
            relative_level = a/(self.typical_range[1]-self.typical_range[0])
        else:
            relative_level = None 
        return relative_level

        
            

def inconsistent_typical_range_stations(stations):
    inconsistent_stations = []
    for station in stations: 
        if station.typical_range_consistent() == False:
            inconsistent_stations.append(station)
    return inconsistent_stations