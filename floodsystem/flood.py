
from .utils import sorted_by_key 
def stations_level_over_threshold(stations, tol):
    "returns list of tuples showing station over tol and relative water level"
    water_level_over_tol = []

    for station in stations:
        a = station.relative_water_level()
        if a != None and a > tol:
            water_level_over_tol.append((station,a))
        else:
            pass
    return sorted_by_key(water_level_over_tol, 1, reverse=True)

def stations_highest_rel_level(stations, N):
    relative_water_level = []
    for station in stations:
        a = station.relative_water_level()
        if a != None:
            relative_water_level.append((station,a))
    b = sorted_by_key(relative_water_level, 1, reverse=True)
    N_highest = b[:N]
    return N_highest



