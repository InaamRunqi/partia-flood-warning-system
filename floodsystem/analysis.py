import matplotlib
import matplotlib.pyplot as plt
import numpy as np

def polyfit(dates,levels,p):
    """Compute a least-squares fit of a polynomial of degree p to water level data."""
    
    x = matplotlib.dates.date2num(dates)
    y = levels

    #Find coefficients of best-fit polynomial f(x) of degree p
    p_coeff = np.polyfit(x-x[-1],y,p)

    #Convert coefficient into a polynomial that can be evaluated
    poly = np.poly1d(p_coeff)
    d0 = x[-1]

    return poly, d0