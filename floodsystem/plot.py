import matplotlib.pyplot as plt
import matplotlib
from .datafetcher import fetch_measure_levels
from .analysis import polyfit
import numpy as np

def plot_water_levels(station,dates,levels):
     """Plot of the water level data against time for a station, including typical low and high levels"""
     
     #Obtain typical low/high values
     low = station.typical_range[0]
     high = station.typical_range[1]

     #Plot
     plt.plot(dates,levels)
     plt.axhline(y=low)
     plt.axhline(y=high)

     #Add axis labels, rotae date labels and add plot title
     plt.xlabel("date")
     plt.ylabel("water level (m)")
     plt.xticks(rotation=45);
     plt.title(station.name)

     #Display plot
     plt.tight_layout()  #This makes sure plot does not cut off data labels
     plt.show()

def plot_water_level_with_fit(station,dates,levels,p):
     """Plot the water level data against time and their best-fit polynomial."""
     
     poly, d0 = polyfit(dates,levels,p)
     x0 = dates
     x = matplotlib.dates.date2num(dates)
     y = levels

     #Obtain typical low/high values
     low = station.typical_range[0]
     high = station.typical_range[1]
     
     # Plot original data points and typical low/high
     plt.plot(x0, y, '.')
     plt.axhline(y=low)
     plt.axhline(y=high)

     #Plot polynomial fit at 30 points along interval
     x1 = np.linspace(x[0], x[-1], 30)
     plt.plot(x1, poly(x1 - d0))

     #Label axis
     plt.xlabel("date")
     plt.ylabel("water level (m)")
     plt.xticks(rotation=45);
     plt.title(station.name)

     # Display plot
     plt.tight_layout()
     plt.show()
