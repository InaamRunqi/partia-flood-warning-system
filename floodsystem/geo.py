# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""

#from floodsystem.utils import sorted_by_key

from .stationdata import build_station_list
from .utils import sorted_by_key  # noqa 
from haversine import haversine


def rivers_with_station(stations):
    """Return a set of rivers with station(s)."""
    river = set()
    for i in range(len(stations)):
        river.add(stations[i].river)
    return river

def stations_by_river(stations):
    """Return a dictionary that maps river names to a list of station objects on a given river."""
    dic = {}
    for i in range(len(stations)):
        if stations[i].river not in dic:
            dic[stations[i].river] = []
            dic[stations[i].river].append(stations[i])
        else:
            dic[stations[i].river].append(stations[i])
    return dic

def rivers_by_station_number(stations,N):
    """
    Return a list of tuples of N rivers with the greatest number of stations.
    If more rivers have the same number of stations as the nth entry, include them all.
    """
    lyst = []
    dic = stations_by_river(stations)
    river = "a"
    for i in range(N):
        num = 0
        for key in dic:
            if len(dic[key]) > num:
                num = len(dic[key])
                river = key
        del dic[river]
        tuplee = (river,num)
        lyst.append(tuplee)
    for key in dic:
        if len(dic[key]) == num:
            tuplee = (key,num)
            lyst.append(tuplee)
    return lyst
        
def stations_by_distance(stations, p):
   "given a list of station objects and a coordinate p, returns a list of (station, distance) tuples"
   station_distance = []
   for station in stations:
      pdistance = haversine (station.coord, p)
      station_distance.append((station, pdistance))
    
   if len(p) != 2:
       raise ValueError ("p must be given as a coordinate")
   else:
       return sorted_by_key(station_distance,1)

def stations_within_radius(stations, centre, r):
   "returns list of stations within radius r of centre"
   stations_in_range = []
   for station in stations:
      cdistance = haversine (station.coord, centre)
      if cdistance<r:
         stations_in_range.append(station)
   
   return stations_in_range

        











