from floodsystem.flood import stations_highest_rel_level
from floodsystem.plot import plot_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from datetime import datetime, timedelta
from floodsystem.stationdata import build_station_list, update_water_levels

def run():
    """Requirements for task 2E."""
    stations = build_station_list()
    update_water_levels(stations)
    stations = stations_highest_rel_level(stations,5)
     
    dt = 10

    for station in stations:
        dates, levels = fetch_measure_levels(station[0].measure_id,dt=timedelta(days=dt))
        plot_water_levels(station[0],dates,levels)
    
if __name__ == "__main__":
    print("*** Task 2E Part 1A Flood Warning System ***")
    
    run()

