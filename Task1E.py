import floodsystem.geo
from floodsystem.stationdata import build_station_list

def run():
    """Requirements for task 1E."""
    stations = build_station_list()
    lyst = floodsystem.geo.rivers_by_station_number(stations,9)
    print(lyst)

if __name__ == "__main__":
    print("*** Task 1E: CUED Part 1A Flood Warning System ***")
    run()

