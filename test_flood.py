from floodsystem.station import MonitoringStation
from floodsystem.stationdata import update_water_levels, build_station_list
from test_station import test_create_monitoring_station
from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level


        

def test_update_water_levels():
    "checks that lastst level is not produced for station with no latest reading"
    a = MonitoringStation(
                                station_id="station_id",
                                measure_id="measure_id",
                                label="label",
                                coord=(50,50),
                                typical_range=(0.2,0.95),
                                river="river",
                                town="town"
                               )
    b =[a]
    update_water_levels(b)
    for station in b:
        assert station.latest_level == None
        assert station.relative_water_level() == None
        

def test_relative_water_levels():
        stations = build_station_list()
        update_water_levels(stations)
        for station in stations:
            a = station.relative_water_level()
            if a != None and a >=0 :
                assert station.latest_level >= station.typical_range[0]   
            else:
                pass
    
station1 = MonitoringStation(
                                station_id="station_id",
                                measure_id="measure_id",
                                label="label",
                                coord=(50,50),
                                typical_range=(1,2),
                                river="river",
                                town="town"
                               )
station2 = MonitoringStation(
                                station_id="station_id",
                                measure_id="measure_id",
                                label="label",
                                coord=(50,50),
                                typical_range=(1,2),
                                river="river",
                                town="town"
                               )
station3 = MonitoringStation(
                                station_id="station_id",
                                measure_id="measure_id",
                                label="label",
                                coord=(50,50),
                                typical_range=(1,5),
                                river="river",
                                town="town"
                               )
station4 = MonitoringStation(
                                station_id="station_id",
                                measure_id="measure_id",
                                label="label",
                                coord=(50,50),
                                typical_range=(1,2),
                                river="river",
                                town="town"
                               )
station1.latest_level = 20
station2.latest_level = None
station3.latest_level = 3
station4.latest_level = 1

def test_stations_over_threshold():
    "checks function works with fake data and gives stations in correct order"
    station1.latest_level = 20
    station2.latest_level = None
    station3.latest_level = 3
    x = [station1,station2,station3]
    y = stations_level_over_threshold(x,0.2)
    assert y[0]== (station1,19)
    assert len(y) == 2

def test_stations_highest_rel_level():
    "checks that only n stations found, and stations are in correct order"
    stations = build_station_list()
    y = stations_highest_rel_level(stations, 10)
    assert len(y) <= 10
    b = []
    for x in y:
        b.append(y[1])
    for a in range (1,len(b)):
        assert b[y]<=b[y-1] 

def test_stations_highest_rel_level_fake_data():
    "checks that invalid data neglected and function works correctly with fake data"
    a = [station1,station2,station3,station4]
    b = stations_highest_rel_level(a,4)
    assert len(b) ==3
    assert b[0] == (station1,19)
    assert b[2] == (station4,0) 

