from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_within_radius

def run():
    """Requirements for Task 1C"""

    # Build list of stations
    stations = build_station_list()

    # create list of stations with in radius 
    a = stations_within_radius(stations, (52.2053, 0.1218), 10)
    b = []
    for station in a:
        b.append(station.name)
    b=sorted(b)
    print(b)



if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")
    run()