from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance

def run():
    """Requirements for Task 1B"""

    # Build list of stations
    stations = build_station_list()
    a = stations_by_distance(stations, (52.2053, 0.1218))
    b = []
     #create list of tuples with station name,town and distance
    for (station, pdistance) in a:
       b.append((station.name, station.town, pdistance))
    
    first10 = b[0:10]
    last10 = b[-11:-1]

    print ("ten closest stations:", first10)
    print("ten furthest stations:",last10) 

if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")
    
    run() 


