from floodsystem.geo import rivers_with_station, stations_by_river, rivers_by_station_number, stations_by_distance, stations_within_radius

from floodsystem.stationdata import build_station_list
from floodsystem.station import MonitoringStation

def test_rivers_with_station():
    """Test rivers_with_station in geo.py"""
    stations = build_station_list()
    river_set = rivers_with_station(stations)
    assert len(river_set) >= 0

def test_stations_by_river():
    """Test stations_by_river in geo.py"""
    stations = build_station_list()
    dic = stations_by_river(stations)
    assert len(dic) >= 0

def test_rivers_by_station_number():
    """Test rivers_by_station_number in geo.py"""
    stations = build_station_list()
    lyst = rivers_by_station_number(stations,1)
    assert len(lyst) >= 0

def test_stations_by_distance():
    """checks that the distance of a station is greater than the station preceeding it"""
    stations = build_station_list()
    a = stations_by_distance(stations, (0,0))
    b = []
    for x in a:
        b.append(x[1])
    for y in range (1,len(b)):
        assert b[y]>=b[y-1]

def test_stations_within_radius():
    stations = build_station_list()
    a = stations_within_radius(stations, (0,0), 5)
    b = stations_within_radius(stations, (0,0), 10)
    assert len(b)>= len(a)















