from datetime import datetime, timedelta
from floodsystem.analysis import polyfit

def test_polyfit():
    """Test polyfit in analysis.py"""
    t = [datetime(2016, 12, 30), datetime(2016, 12, 31), datetime(2017, 1, 1),
         datetime(2017, 1, 2), datetime(2017, 1, 3), datetime(2017, 1, 4),
         datetime(2017, 1, 5)]
    level = [0.2, 0.7, 0.95, 0.92, 1.02, 0.91, 0.64]
    poly, d0 = polyfit(t,level,1)
    assert poly(0.1) >= 0
    assert d0 >= 0
    

